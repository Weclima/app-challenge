import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page {
  menu = [
    {
      src: 'assets/tab2/img9tab2.png',
      item: 'COLEÇÃO',
    },
    {
      src: 'assets/tab2/img6tab2.png',
      item: 'NOVIDADES',
    },
    {
      src: 'assets/tab2/img13tab2.png',
      item: 'ASSESSORIOS',
    },
    {
      src: 'assets/tab2/img7tab2.png',
      item: 'INTIMATES',
    },
    {
      src: 'assets/tab2/img8tab2.png',
      item: 'SALE',
    },
    {
      src: 'assets/tab2/img11tab2.png',
      item: 'ANIMALE ORO',
    },
    {
      src: 'assets/tab2/img12tab2.png',
      item: 'INSIDE ANIMALE',
    },
  ];
  constructor() {}
}
