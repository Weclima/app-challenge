import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsModule } from '../components/components.module';
import { Tab4Page } from './tab4.page';

const routes: Routes = [
  {
    path: '',
    component: Tab4Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
  ComponentsModule
  ],
  exports: [RouterModule],
})
export class Tab4PageRoutingModule {}
